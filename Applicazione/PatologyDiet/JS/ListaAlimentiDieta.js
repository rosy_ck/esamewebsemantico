$(document).ready(function(){
  var Patologia=getCookie("PatologiaScelta");
  var queryS="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
  "PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#> "+
"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
"SELECT  ?alimentiConsigliati  ?nomeAlimento "+
"from <http://progetto/PatologyDiet>"+
"  WHERE { patologyDiet:"+Patologia+
" patologyDiet:Consigliata ?dieta."+
" ?dieta    patologyDiet:AlimentiConsigliati  ?alimentiConsigliati."+
" ?alimentiConsigliati patologyDiet:NomeAlimento ?nomeAlimento. ?alimentiSConsigliati patologyDiet:NomeAlimento ?nomeAlimento."+
" } order by ?alimentiConsigliati";
console.log(queryS);
  $.ajax({
    url: "http://localhost:7200/repositories/PatologyDiet_rosy",
             crossDomain: true,
             type:"POST",
             data:{'query':queryS,'infer':true,'sameAs':true},
            dataType: 'json',
           //header:{"Accept":"application/rdf+json"},
            success: function(data) {
              $.each(data["results"]["bindings"],function(i,item){
               $(".list-group.Consigliati").append("<li class=\"list-group-item clickMe\" id=\""+item["alimentiConsigliati"]["value"]+"\">"+item["nomeAlimento"]["value"]+"</li>");
              });
            }
          });
 queryS="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
          "PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#>"+
        "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
        "SELECT  ?alimentiSconsigliati  ?nomeAlimento  "+
        "from <http://progetto/PatologyDiet>"+
        "  WHERE {  patologyDiet:"+Patologia+
        " patologyDiet:Consigliata ?dieta."+
        " ?dieta    patologyDiet:AlimentiDaEvitare  ?alimentiSconsigliati."+
        " ?alimentiSconsigliati patologyDiet:NomeAlimento ?nomeAlimento. ?alimentiSconsigliati patologyDiet:NomeAlimento ?nomeAlimento."+
        " } order by ?alimentiSconsigliati";
          $.ajax({
            url: "http://localhost:7200/repositories/PatologyDiet_rosy",
                     crossDomain: true,
                     type:"POST",
                     data:{'query':queryS,'infer':true,'sameAs':true},
                    dataType: 'json',
                   //header:{"Accept":"application/rdf+json"},
                    success: function(data) {
                      $.each(data["results"]["bindings"],function(i,item){
                       $(".list-group.Sconsigliati").append("<li class=\"list-group-item clickMe\" id=\""+item["alimentiSconsigliati"]["value"]+"\">"+item["nomeAlimento"]["value"]+"</li>");
                      });
                    }
                  });

});
$(document).on("click",".clickMe",function(){
  var  urlAlimento=$(this).attr("id").replace("http://www.semanticweb.org/rosi/ontologies/","").replace("PatologyDiet#","patologyDiet:");
  window.location="GetInfoAlimento.php?Alimento="+urlAlimento;
});
