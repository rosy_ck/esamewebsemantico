var currentDate=getUrlParameter("Data");
var Patologia=getCookie("PatologiaScelta");
$(document).ready(function(){
  var queryS="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
  "PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#>"+
"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
"SELECT ?patologia ?alimentiConsigliati  ?nomeAlimento ?dieta  ?tipoPiatto "+
" from <http://progetto/PatologyDiet>"+
"  WHERE { patologyDiet:"+Patologia+
" patologyDiet:Consigliata ?dieta."+
"?alimentiConsigliati patologyDiet:Adeguato ?dieta    ."+
"?alimentiConsigliati  rdf:type ?tipoPiatto; patologyDiet:NomePiatto ?nomeAlimento. ?tipoPiatto rdfs:subClassOf*   patologyDiet:Piatto "+
" }";
console.log(queryS);
var jsonALimentiConsigliati
var repString="http://www.semanticweb.org/rosi/ontologies/PatologyDiet#";
  $.ajax({
    url: "http://localhost:7200/repositories/PatologyDiet_rosy",
             crossDomain: true,
             type:"POST",
             data:{'query':queryS,'infer':false,'sameAs':true},
            dataType: 'json',
           //header:{"Accept":"application/rdf+json"},
            success: function(data) {
            document.cookie="DietaConsigliata="+ data["results"]["bindings"][0]["dieta"]["value"];
              $.each(data["results"]["bindings"],function(i,item){
                    if((item["tipoPiatto"]["value"].indexOf('Primo')!=-1)||(item["tipoPiatto"]["value"].indexOf('Secondo')!=-1)){
                      $(".Primo select").append("<option id=\""+item["alimentiConsigliati"]["value"].replace(repString,"patologyDiet:")+"\">"+item["nomeAlimento"]["value"]+"</option>");
                    }else if(item["tipoPiatto"]["value"].indexOf('Contorno')!=-1){
                      $(".Secondo select").append("<option id=\""+item["alimentiConsigliati"]["value"].replace(repString,"patologyDiet:")+"\">"+item["nomeAlimento"]["value"]+"</option>");
                    }else if(item["tipoPiatto"]["value"].indexOf('Dessert')!=-1){
                      $(".Dessert select").append("<option id=\""+item["alimentiConsigliati"]["value"].replace(repString,"patologyDiet:")+"\">"+item["nomeAlimento"]["value"]+"</option>");
                    }else if(item["tipoPiatto"]["value"].indexOf('Bevande')!=-1  ){
                          $("#Colazione_Bevande").append("<option id=\""+item["alimentiConsigliati"]["value"].replace(repString,"patologyDiet:")+"\">"+item["nomeAlimento"]["value"]+"</option>");
                    } else if((item["tipoPiatto"]["value"].indexOf('AlimentoPerColazione')!=-1 )){
                    var  isDessert=false;
                      for(var j=0;j<data["results"]["bindings"].length;j++){
                        if( item["alimentiConsigliati"]["value"].indexOf(data["results"]["bindings"][j]["alimentiConsigliati"]["value"])!=-1){
                          if (data["results"]["bindings"][j]["tipoPiatto"]["value"].indexOf('Dessert')!=-1) {isDessert=true;break;}
                        }
                        }
                        if (isDessert){
                      $("#Colazione_Frutta").append("<option id=\""+item["alimentiConsigliati"]["value"].replace(repString,"patologyDiet:")+"\">"+item["nomeAlimento"]["value"]+"</option>");
                    }  else{
                      $("#Colazione_primo").append("<option id=\""+item["alimentiConsigliati"]["value"].replace(repString,"patologyDiet:")+"\">"+item["nomeAlimento"]["value"]+"</option>");
                    }

                    }
              });
              }
          });

          $("button").click(function(e){
             e.preventDefault();
             var Menu={};
             Menu["colazione"]={};
             Menu["colazione"]["bevanda"]={};
              Menu["colazione"]["primo"]={};
              Menu["colazione"]["frutta"]={};
             console.log($("#Colazione_Bevande :selected").text());
            Menu["colazione"]["bevanda"]["nome"]=$("#Colazione_Bevande :selected").attr("id");
               Menu["colazione"]["bevanda"]["peso"]=$("#Colazione_Bevande").parents(".Colazione").find("input").val();
             Menu["colazione"]["primo"]["nome"]=$("#Colazione_primo  :selected").attr("id");
             Menu["colazione"]["primo"]["peso"]=$("#Colazione_primo").parents(".Colazione").find("input").val();
              Menu["colazione"]["frutta"]["nome"]=$("#Colazione_Frutta  :selected").attr("id");
                   Menu["colazione"]["frutta"]["peso"]=$("#Colazione_Frutta").parents(".Colazione").find("input").val();
              Menu["pranzo"]={};
                 Menu["pranzo"]["primo"]={};
                 Menu["pranzo"]["secondo"]={};
                 Menu["pranzo"]["dessert"]={};
              Menu["pranzo"]["primo"]["nome"]=$("#Pranzo_Primo  :selected").attr("id");
               Menu["pranzo"]["primo"]["peso"]=$("#Pranzo_Primo").parents(".Primo").find("input").val();
               Menu["pranzo"]["secondo"]["nome"]=$("#Pranzo_Secondo  :selected").attr("id");
                   Menu["pranzo"]["secondo"]["peso"]=$("#Pranzo_Secondo").parents(".Secondo").find("input").val();
                Menu["pranzo"]["dessert"]["nome"]=$("#Pranzo_Dessert  :selected").attr("id");
                 Menu["pranzo"]["dessert"]["peso"]=$("#Pranzo_Dessert ").parents(".Dessert").find("input").val();
                Menu["cena"]={};
                Menu["cena"]["primo"]={};
                Menu["cena"]["secondo"]={};
               Menu["cena"]["dessert"]={};
                Menu["cena"]["primo"]["nome"]=$("#Cena_Primo  :selected").attr("id");
                Menu["cena"]["primo"]["peso"]=$("#Cena_Primo").parents(".Primo").find("input").val();
                 Menu["cena"]["secondo"]["nome"]=$("#Cena_Secondo  :selected").attr("id");
                   Menu["cena"]["secondo"]["peso"]=$("#Cena_Secondo").parents(".Secondo").find("input").val();
                  Menu["cena"]["dessert"]["nome"]=$("#Cena_Dessert  :selected").attr("id");
                   Menu["cena"]["dessert"]["peso"]=$("#Cena_Dessert").parents(".Dessert").find("input").val();

                //   if(Validate()){
                //  alert( validateJSON(json));
                var DietaConsigliata =getCookie("DietaConsigliata").replace("http://www.semanticweb.org/rosi/ontologies/PatologyDiet#","patologyDiet:");
          if(DietaConsigliata){
            //creareMenucolazione
          var InsertMenu=  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
          +" PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#>"
          +" PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
          +" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
          +" INSERT DATA{GRAPH <http://progetto/PatologyDiet> {patologyDiet:colazione"+currentDate+" rdf:type patologyDiet:Colazione;"
          +" patologyDiet:Giorno \""+currentDate+"\"^^xsd:string. patologyDiet:MenuColazione"+currentDate+" rdf:type patologyDiet:MenuColazione;"
          +" patologyDiet:MenuPerDieta "+ DietaConsigliata+";"
          +" patologyDiet:Durante patologyDiet:colazione"+currentDate+"."
          +" patologyDiet:Pranzo"+currentDate+" rdf:type patologyDiet:Pranzo;"
          +" patologyDiet:Giorno \""+currentDate+"\"^^xsd:string."
          +" patologyDiet:MenuPranzo"+currentDate+" rdf:type patologyDiet:MenuPranzo;"
          +" patologyDiet:Durante patologyDiet:Pranzo"+currentDate+" ."
          +" patologyDiet:MenuPranzo"+currentDate+" patologyDiet:MenuPerDieta "+ DietaConsigliata+"."
          +" patologyDiet:Cena"+currentDate+" rdf:type patologyDiet:Cena;"
          +" patologyDiet:Giorno \""+currentDate+"\"^^xsd:string. patologyDiet:MenuCena"+currentDate+" rdf:type patologyDiet:MenuCena;"
          +" patologyDiet:MenuPerDieta "+ DietaConsigliata+";"
          +" patologyDiet:Durante patologyDiet:Cena"+currentDate+"."
          //Per ogni piatto Inserisci la voce
          for(var key in Menu["colazione"]){
            //addVoceMenu
             InsertMenu +=" patologyDiet:Menu_V_"+Menu["colazione"][key]["nome"].replace(" ","_").replace("patologyDiet:","")+" rdf:type patologyDiet:Voce_Menu;"
           +" patologyDiet:TipoPiatto "+Menu["colazione"][key]["nome"]+";"
          +  " patologyDiet:Peso \""+Menu["colazione"][key]["peso"]+"\"^^xsd:float."
            +" patologyDiet:Menucolazione"+currentDate+" patologyDiet:FormatoDa  patologyDiet:Menu_V_"+Menu["colazione"][key]["nome"].replace(" ","_").replace("patologyDiet:","")+"."
          }
          for(var key in Menu["pranzo"]){
            //addVoceMenu
             InsertMenu +=" patologyDiet:Menu_V_"+Menu["pranzo"][key]["nome"].replace(" ","_").replace("patologyDiet:","")+" rdf:type patologyDiet:Voce_Menu;"
           +" patologyDiet:TipoPiatto "+Menu["pranzo"][key]["nome"]+";"
          +  " patologyDiet:Peso \""+Menu["pranzo"][key]["peso"]+"\"^^xsd:float."
            +" patologyDiet:MenuPranzo"+currentDate+" patologyDiet:FormatoDa  patologyDiet:Menu_V_"+Menu["pranzo"][key]["nome"].replace(" ","_").replace("patologyDiet:","")+"."
          }
          for(var key in Menu["cena"]){
            //addVoceMenu
             InsertMenu +=" patologyDiet:Menu_V_"+Menu["cena"][key]["nome"].replace(" ","_").replace("patologyDiet:","")+" rdf:type patologyDiet:Voce_Menu;"
           +" patologyDiet:TipoPiatto  patologyDiet:"+Menu["cena"][key]["nome"]+";"
          +  " patologyDiet:Peso \""+Menu["cena"][key]["peso"]+"\" ^^xsd:float. "
            +" patologyDiet:MenuCena"+currentDate+" patologyDiet:FormatoDa  patologyDiet:Menu_V_"+Menu["cena"][key]["nome"].replace(" ","_").replace("patologyDiet:","")+"."
          }
          InsertMenu += "}}";
          $.ajax({
              url: "http://localhost:7200/repositories/PatologyDiet_rosy/statements",
                       crossDomain: true,
                       type:"POST",
                       data:{'update':InsertMenu},
                      //dataType: 'json',
                      contentType:"application/x-www-form-urlencoded",
                      success:function() { $("body form").hide();    $("body").append(   "<div class=\"alert alert-success\" role=\"alert\">  Il menu è stato aggiunto con successo!    </div>");   $(".alert.alert-success").show();},
                      error:function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        alert('Error - ' + errorMessage);
                           $("body form").hide();
                                $("body").append(   "<div class=\"alert alert-danger\" role=\"alert\">  Il menu non è stato aggiunto! ErrorMessage   </div>");
                                   $(".alert.alert-danger").show();
                                 }
                    });
                  }
        });
});
function Validate()
{
    var msg= "",
        fields = document.getElementById("crea_Menu").getElementsByTagName("input");

    for (var i=0; i<fields.length; i++){
        if (fields[i].value == "")
            msg += fields[i].title + ' is required. \n';
    }

    if(msg) {
        alert(msg);
        return false;
    }
    else
        return true;
}
